#! /bin/bash

# check to avoid multiple service spawn

pid=$(ps ax | grep -i 'org\.waarp\.openr66\.proxy\.R66Proxy' | grep -v grep | awk '{print $1}')
        
if [ "k$pid" != "k" ]; then
	echo "waarp proxy already running (pid $pid) ..."
	exit 0
fi

echo Start R66Server
CONFIG_FILE=$1
. {{work_path}}/R66/ENV_R66
nohup ${JAVARUNSERVER} org.waarp.openr66.proxy.R66Proxy ${CONFIG_FILE} &> ${R66HOME}/log/R66Server.out &
#set MYPID=$!
#sleep 60
#MYTEST=`ps -eaf | grep -G '^\s*'${MYPID} | grep -v grep | grep openr66.server.R66Server`
#if [ "${MYTEST}" != "" ] then 
#	echo ${MYPID} > ${R66HOME}/log/lastpid
#	echo R66Server started
#else
#	echo R66Server failed to start
#fi
